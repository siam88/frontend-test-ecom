import React, { createContext, useState } from 'react';
import axios from "axios";

export const ProductContext = createContext();
const ProductContextProvider = ({ children }) => {

    const [products, setProducts] = useState(null);
    const [productInfo, setProductInfo] = useState(null);


    const getAllProducts = async () => {
        try {
            const res = await axios.get("http://localhost:3002/products");
            setProducts(res.data);
            return true;
        } catch (error) {
            console.log("products loading error.", error.response ? error.response.message : error.message)
            return false;
        }

    }





    return (
        <ProductContext.Provider
            value={{
                products,
                getAllProducts,

            }}
        >
            {children}
        </ProductContext.Provider>
    );
}

export default ProductContextProvider;