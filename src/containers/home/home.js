import React, { useState, useEffect, useContext, Fragment } from "react";
import axios from 'axios';
import * as classes from './home.module.css';
import { ProductContext } from '../../contexts/productContextProvider'


const Home = () => {


    const productContext = useContext(ProductContext);

    async function getAllProducts() {
        const status = await productContext.getAllProducts();
    }

    useEffect(() => {
        getAllProducts();
    }, [])

    var url = "http://localhost:3002/";

    return (
        <div style={{ display: "flex" }}>

            {productContext.products ? productContext.products.map((e, i) => {
                return (<div className={classes.card}>
                    <img src={url.concat(e.image)} alt="Avatar" className={classes.img} />
                    <div className={classes.container}>
                        <h4 style={{ color: "#4D4D4D" }}><b>{e.name}</b></h4>
                        <div className={classes.cardFooter} >
                            <p>{e.ProductPrice - (e.ProductPrice * e.discountRate / 100)}BDT</p>
                            <p style={{ backgroundColor: "yellow", padding: "1% 5%" }}>{e.discountRate}%</p>
                        </div>

                    </div>
                </div>)
            }) : "loading..."}


        </div>)
}

export default Home