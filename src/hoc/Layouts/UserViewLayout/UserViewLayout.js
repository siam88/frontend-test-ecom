import React, { Component, Fragment } from 'react';
import SearchIcon from '@material-ui/icons/Search';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import PermIdentityOutlinedIcon from '@material-ui/icons/PermIdentityOutlined';


import classes from './UserViewLayout.module.css'
class UserViewLayout extends Component {
    render() {
        return (
            <Fragment>
                <header className={classes.topnav}>
                    <div className={classes.logo}>
                        <h4 className={classes.logo}>Test Logo</h4>
                    </div>
                    <div className={classes.logo}>
                        <input type="text" style={{ float: "left" }} className={classes.search} placeholder="search…" />
                    </div>
                    <ul className={classes.navLinks}>
                        <li>
                            <a href="#"><ShoppingCartOutlinedIcon /> Cart </a>
                        </li>
                        <li><a href="#"><PermIdentityOutlinedIcon /></a></li>


                    </ul>
                    <div className={classes.threeline}>
                        <div className={classes.line1}></div>
                        <div className={classes.line2}></div>
                        <div className={classes.line3}></div>

                    </div>
                </header>
                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </Fragment>
        );
    }
}

export default UserViewLayout

/**
 *  <ul>
                        <li style={{ marginLeft: "83px" }}>
                            <p className={classes.logo}>Test Logo</p>
                        </li>
                        <li style={{ display: "inline-flex", borderBottom: "1px solid black", marginLeft: "539px" }}>
                            <div style={{ marginTop: "15px" }}>
                                <SearchIcon fontSize="inherit" />
                            </div>
                            <div>
                                <input type="text" style={{ float: "left" }} className={classes.search} placeholder="search…" />
                            </div>
                        </li>
                        <li >
                            <div style={{ marginTop: "15px" }}>
                                <ShoppingCartOutlinedIcon fontSize="inherit" />
                            </div>
                        </li>
                        <li >
                            <div style={{ marginTop: "15px" }}>
                                <ShoppingCartOutlinedIcon fontSize="inherit" />
                            </div>
                        </li>
                    </ul>
 */